import Home from "./Home.svelte";
import Calculator from "./Calculator.svelte";
import NotFound from "./NotFound.svelte";
import CalcResult from "./CalcResult.svelte";
import Map from "./Map.svelte";
import Wiki from "./Wiki.svelte";

const routes = {
  "/": Home,
  "/calculator/": Calculator,
  "/calc-result/": CalcResult,
  "/map/": Map,
  "/wiki/": Wiki,
  "*": NotFound
};

export default routes;
